<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Libraries\ImageProcessing;

class MainController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function resizeFile($file, $width, $height, $method, $format = null)
    {
        $image = new ImageProcessing();
        $image->load($file);

        switch ($method) {
            case 'scaleWidth':
                $image->{$method}($width);
                break;
            case 'scaleHeight':
                $image->{$method}($height);
                break;
            default:
                $image->{$method}($width, $height);
                break;
        }

        if (true === is_null($format)) {
            $image->save($file);

            return $file;
        }

        $image->save($file, $format);

        return $file;
    }

    public function store(Request $request)
    {
        $data = json_decode($request->data, true);

        if (false === strpos($request->file, 'http')) {
            $request->file->move(public_path('images'), $request->name);
        } else {
            file_put_contents(
                public_path('images') . DIRECTORY_SEPARATOR . $request->name,
                file_get_contents($request->file)
            );
        }

        $this->resizeFile(
            public_path('images') . DIRECTORY_SEPARATOR . $request->name,
            $data['width'],
            $data['height'],
            $data['selectedMethod']
        );

        return response()->json(['success' => 'You have successfully uploaded and resized image.']);
    }

}
