<?php

namespace Libraries;

use Libraries\Interfaces\ImageResizerInterface;

class ImageProcessing implements ImageResizerInterface
{
    private $image;
    private $imageType;

    function load($filename)
    {
        if (false === file_exists($filename)) {
            return false;
        }

        $image_info      = getimagesize($filename);
        $this->imageType = $image_info[2];
        if ($this->imageType == IMAGETYPE_JPEG) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->imageType == IMAGETYPE_GIF) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->imageType == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
        }

        return true;
    }

    function save($filename, $imageType = IMAGETYPE_JPEG, $compression = 75, $permissions = null)
    {
        if ($imageType == IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $compression);
        } elseif ($imageType == IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($imageType == IMAGETYPE_PNG) {
            imagepng($this->image, $filename);
        }
        if ($permissions != null) {
            chmod($filename, $permissions);
        }
    }

    function output($imageType = IMAGETYPE_JPEG)
    {
        if ($imageType == IMAGETYPE_JPEG) {
            imagejpeg($this->image);
        } elseif ($imageType == IMAGETYPE_GIF) {
            imagegif($this->image);
        } elseif ($imageType == IMAGETYPE_PNG) {
            imagepng($this->image);
        }
    }

    private function getWidth()
    {
        return imagesx($this->image);
    }

    private function getHeight()
    {
        return imagesy($this->image);
    }

    public function scaleHeight($height)
    {
        $ratio       = $this->getHeight() / $height;
        $targetWidth = $this->getWidth() / $ratio;
        if ($height > $this->getHeight()) {
            $ratio       = $height / $this->getHeight();
            $targetWidth = $this->getWidth() * $ratio;
        }

        $targetHeight = $height;

        $newImage = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $targetWidth, $targetHeight, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;
    }

    public function scaleWidth($width)
    {
        $ratio        = $this->getWidth() / $width;
        $targetHeight = $this->getHeight() / $ratio;
        if ($width > $this->getWidth()) {
            $ratio        = $width / $this->getWidth();
            $targetHeight = $this->getHeight() * $ratio;
        }

        $targetWidth = $width;

        $newImage = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $targetWidth, $targetHeight, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;
    }

    public function crop($newWidth, $newHeight)
    {
        $this->fit($newWidth, $newHeight, true);
    }

    public function fit($newWidth, $newHeight, $crop = false)
    {
        $realWidth  = $newHeight / $this->getHeight();
        $realHeight = $newWidth / $this->getWidth();

        $realRatio = min($realWidth, $realHeight);

        if (true === $crop) {
            $realRatio = max($realWidth, $realHeight);
        }

        $newHeightInternal = round(imagesy($this->image) * $realRatio);
        $newWidthInternal  = round(imagesx($this->image) * $realRatio);

        $secondImage = $this->image;
        $this->image = imagecreatetruecolor($newWidthInternal, $newHeightInternal);
        imagecopyresampled($this->image, $secondImage, 0, 0, 0, 0, $newWidthInternal, $newHeightInternal, imagesx($secondImage), imagesy($secondImage));

        $secondImage = $this->image;
        $this->image = imagecreatetruecolor($newWidth, $newHeight);
        imagefill($this->image, 0, 0, imagecolorallocatealpha($this->image, 0, 0, 0, 127));

        $destinationX = 0;
        $destinationY = 0;
        if (imagesx($this->image) > imagesx($secondImage)) {
            $destinationX = round((imagesx($this->image) - imagesx($secondImage)) / 2);
        }

        if (imagesy($this->image) > imagesy($secondImage)) {
            $destinationY = round((imagesy($this->image) - imagesy($secondImage)) / 2);
        }

        if (true === $crop) {
            if (imagesx($this->image) < imagesx($secondImage)) {
                $destinationX = round((imagesx($this->image) - imagesx($secondImage)) / 2);
            }

            if (imagesy($this->image) < imagesy($secondImage)) {
                $destinationY = round((imagesy($this->image) - imagesy($secondImage)) / 2);
            }
        }

        imagecopy($this->image, $secondImage, $destinationX, $destinationY, 0, 0, imagesx($secondImage), imagesy($secondImage));
        imagedestroy($secondImage);
    }

    function fill($targetWidth, $targetHeight, $keepRatio = true)
    {
        if (true === $keepRatio) {
            $ratio = $this->getWidth() / $this->getHeight();

            if ($targetWidth / $targetHeight > $ratio) {
                $targetWidth = $targetHeight * $ratio;
            } else {
                $targetHeight = $targetWidth / $ratio;
            }
        }

        $newImage = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $targetWidth, $targetHeight, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

}
