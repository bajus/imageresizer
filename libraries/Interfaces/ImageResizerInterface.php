<?php

namespace Libraries\Interfaces;

interface ImageResizerInterface
{
    public function fit($width, $height);

    public function fill($width, $height);

    public function scaleWidth($width);

    public function scaleHeight($height);

    public function crop($width, $height);
}
