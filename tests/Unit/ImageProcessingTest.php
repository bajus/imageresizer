<?php

namespace Tests\Unit;

use Libraries\ImageProcessing;
use PHPUnit\Framework\TestCase;

use Faker\Factory as Faker;

class ImageProcessingTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testLoadWithFakeData()
    {
        $faker = Faker::create();

        $imageProcessing = new ImageProcessing();
        $this->assertFalse($imageProcessing->load($faker->word));
    }

    private function createImage()
    {
        return imagecreatetruecolor(20, 20);
    }

    public function testLoadWithRealData()
    {
        $faker = Faker::create();

        $fileName = $faker->word;

        $imageProcessing = new ImageProcessing();
        $file            = $this->createImage();
        imagejpeg($file, $fileName, 90);
        $this->assertTrue($imageProcessing->load($fileName));
        unlink($fileName);
    }

    public function testSave()
    {
        $faker = Faker::create();

        $fileName = $faker->word;

        $imageProcessing = new ImageProcessing();
        imagejpeg($this->createImage(), $fileName, 90);
        $imageProcessing->load($fileName);
        $imageProcessing->save($fileName);

        $this->assertFileExists($fileName);
        unlink($fileName);
    }

    public function testFit()
    {
        $faker = Faker::create();

        $fileName = $faker->word;

        $imageProcessing = new ImageProcessing();
        imagejpeg($this->createImage(), $fileName, 90);
        $imageProcessing->load($fileName);

        $width  = 100;
        $height = 100;

        $imageProcessing->fit($width, $height);

        $this->assertEquals($width, imagesx($imageProcessing->getImage()));
        $this->assertEquals($height, imagesy($imageProcessing->getImage()));
        unlink($fileName);
    }

    public function testCrop()
    {
        $faker = Faker::create();

        $fileName = $faker->word;

        $imageProcessing = new ImageProcessing();
        imagejpeg($this->createImage(), $fileName, 90);
        $imageProcessing->load($fileName);

        $width  = 100;
        $height = 100;

        $imageProcessing->crop($width, $height);

        $this->assertEquals($width, imagesx($imageProcessing->getImage()));
        $this->assertEquals($height, imagesy($imageProcessing->getImage()));
        unlink($fileName);
    }
}
